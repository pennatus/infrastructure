# Uncomment out the following block when Vault is setup and a token is available
# Make sure to also update the locals block to use the data.vault_generic secret source

# provider "vault" {
#   address = var.VAULT_ADDRESS
#   token = var.VAULT_TOKEN
# }
# data "vault_generic_secret" "gcp_token" {
#   path = "/org/gcp/token/provisioner"
# }

locals {
# Uncomment the assignment to data.vault_generic_secret and comment out the
# assignment to the variable GCP_ACCESS_TOKEN when the provisioner Vault token is being used.

    #GCP_ACCESS_TOKEN = data.vault_generic_secret.gcp_token.data["token"]
    GCP_ACCESS_TOKEN = var.GCP_ACCESS_TOKEN
}

###################################################################################################


provider "google" {
  access_token = local.GCP_ACCESS_TOKEN
}

provider "google-beta" {
  access_token = local.GCP_ACCESS_TOKEN
}

# Require cloud billing because once the Vault token is being used to grab a provisioner
# GCP service account it will be running from inside a project.  When running inside a proj
resource "google_project_service" "resourcemanagerapi" {
  project = var.ADMIN_PROJECT
  service = "cloudbilling.googleapis.com"
}


module "vault_on_gcloud_1" {
  source = "git::https://gitlab.com/pennatus/infrastructure//modules/vault-on-gcloud"

  ADMIN_PROJECT = var.ADMIN_PROJECT
  GCP_ACCESS_TOKEN = local.GCP_ACCESS_TOKEN
  DOMAIN = var.DOMAIN
}


module "org_folders" {
  source = "git::https://gitlab.com/pennatus/infrastructure//modules/org-folders"

  DOMAIN = var.DOMAIN
  ENVIRONMENTS = var.ENVIRONMENTS
}

# Enable policies after Vault is running
# module "org_policies" {
#   source = "git::https://gitlab.com/pennatus/infrastructure//modules/org-policies"
#   ENABLE_VAULT = false
#   ENABLE_DB_INSTANCE = false
#   ENABLE_ORG = false
#   ENABLE_PRODUCT = false
#   ENABLE_TOKEN = false
#   ENVIRONMENTS = var.ENVIRONMENTS
#   OIDC_DEFAULT_POLICY = var.OIDC_DEFAULT_POLICY
# }