# resource "vault_jwt_auth_backend" "oidc_auth_backend" {
#   count = length(var.VAULT_TOKEN) == 0 ? 0: length(var.OIDC_CLIENT_ID) == 0 ? 0: length(var.OIDC_CLIENT_SECRET) == 0 ? 0 : 1
#   description  = "GCP OIDC Backend"
#   path = "oidc"
#   type = "oidc"
#   oidc_discovery_url = "https://accounts.google.com"
#   oidc_client_id = var.OIDC_CLIENT_ID
#   oidc_client_secret = var.OIDC_CLIENT_SECRET
#   default_role = var.OIDC_DEFAULT_POLICY
# }

# resource "vault_jwt_auth_backend_role" "oidc_auth_role_default" {
#   count = length(var.VAULT_TOKEN) == 0 ? 0: length(var.OIDC_CLIENT_ID) == 0 ? 0: length(var.OIDC_CLIENT_SECRET) == 0 ? 0 : 1
#   backend         = vault_jwt_auth_backend.oidc_auth_backend[count.index].path
#   role_name       = var.OIDC_DEFAULT_POLICY
#   token_policies  = ["default", var.OIDC_DEFAULT_POLICY]
#   user_claim      = "sub"
#   role_type       = "oidc"
#   allowed_redirect_uris = ["${var.VAULT_ADDRESS}/ui/vault/auth/oidc/oidc/callback", "http://localhost:8250/oidc/callback"]
#   bound_audiences = [var.OIDC_CLIENT_ID]
# }
