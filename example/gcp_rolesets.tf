// This file is used to specify optional extra service accounts that Vault can generate tokens for.
//
// gcp_org_vault_run_admin is used to allocate a service account that has roles/run.admin and roles/iam.serviceAccountUser on the
//   cloud run instance for Vault.

# resource "vault_gcp_secret_roleset" "gcp_org_vault_run_admin" {
#   backend      = "/org/gcp"
#   roleset      = "org-vault-run-admin"
#   secret_type  = "access_token"
#   project      = var.ADMIN_PROJECT
#   token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

#   binding {
#     resource = "//cloudresourcemanager.googleapis.com/projects/${var.ADMIN_PROJECT}"

#     roles = [
#       "roles/run.admin",
#       "roles/iam.serviceAccountUser"  # Needed to impersonate the cloud run service account when modifying the run service
#     ]
#   }
# }
# output "gcp_org_vault_run_admin_path" {
#   value = "${vault_gcp_secret_roleset.gcp_org_vault_run_admin.backend}/token/${vault_gcp_secret_roleset.gcp_org_vault_run_admin.roleset}"
# }
