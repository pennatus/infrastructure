# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# These parameters must be supplied but should be in the autofars.tfvars file.
# ---------------------------------------------------------------------------------------------------------------------

variable "DOMAIN" {
  description = "Domain that is hosting the account"
  type        = string
}

variable "ADMIN_PROJECT" {
  description = "This is the infrastructure project to deploy vault under - project id"
  type        = string
}

# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# These parameters must be supplied and are secretive
# ---------------------------------------------------------------------------------------------------------------------

variable "GCP_ACCESS_TOKEN" {
  description = "Short lived GCP access token.  Get using `gcloud auth print-access-token`"
  default = ""
}

variable "VAULT_TOKEN" {
  description = "High level access Vault token - administrator level access"
  type        = string
  default     = ""   # Providing a default until Vault is running after which time this will be required
}

variable "VAULT_ADDRESS" {
  description = "This is the domain given to you by Cloud Run and can be found in the console."
  type        = string
  default     = ""   # Providing a default until Vault is running after which time this will be required
}

variable "OIDC_CLIENT_ID" {
  description = "After setting up access credentials in is-org this is the OIDC Client ID for the application.  Specify this if you want to offer your users OIDC login access."
  type        = string
  default     = ""
}

variable "OIDC_CLIENT_SECRET" {
  description = "After setting up access credentials in is-org this is the OIDC Client Secret for the application.  Specify this if you want to offer your users OIDC login access."
  type        = string
  default     = ""
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# These parameters can be specified to override defaults.
# ---------------------------------------------------------------------------------------------------------------------


variable "ENVIRONMENTS" {
  description = "List of possible environments for all projects"
  type = list
  default = ["dev", "staging", "prod"]
}

variable "OIDC_DEFAULT_POLICY" {
  description = "This is the default Vault Policy to give users that successfully authenticate using OIDC."
  type        = string
  default     = "team-viewer"
}


######################################################
# List of billing accounts to associate products with
######################################################

variable "BILLING_NAME_GENERAL" {
  description = "Display name for a billing account"
  type = string
  default = "My Billing Account"
}

