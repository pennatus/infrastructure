# Deploy Website Product
#  Uncomment when everything in main.tf is done, i.e vault deployed, gcp secret engines deployed, policies deployed

# module "product_shared" {
#   source = "git::https://gitlab.com/pennatus/infrastructure//modules/product-factory"

#   PRODUCT_NAME = "shared"
#   ENVIRONMENTS = [ "prd" ]
#   APIS = [ ]
#   SSH_KEYS = [ "sgraham: ssh-rsa AAAAB3Nza****HlGKiR5dP2IJ5Guw== sgraham@cabsdg" ]
#   KV_SECRET_PATHS = [ ]

#   ADMIN_PROJECT = var.ADMIN_PROJECT
#   BILLING_DISPLAY_NAME = var.BILLING_NAME_GENERAL
#   DOMAIN = var.DOMAIN
#   FOLDER_IDS = module.org_folders.folder_ids
# }
# output shared_ids {
#   value = module.product_shared.env_project_map
# }