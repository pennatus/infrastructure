variable "ENVIRONMENTS" {
  description = "List of possible environments, use the global value."
  type = list
}

variable "OIDC_DEFAULT_POLICY" {
  description = "Name of the default OIDC policy (ex: team-viewer)"
  type = string
}

variable "ENABLE_VAULT" {
  description = "Define classes for Vault, the admin class allows the token holder to manage all aspects of Vault without being full root.  The owner class is modelled after Terraforms example of a provisioner which is intended to be a hybrid of an owner and admin.  Classes: admin, owner"
  type = bool
  default = false
}

variable "ENABLE_TOKEN" {
  description = "Token creator is required if a vault provider is used and grants the permission to create a child token from the issued token.  For example, the token issued to a terraform.io workspace should be granted the token-create policy.  Classes: creator"
  type = bool
  default = true
}

variable "ENABLE_PRODUCT" {
  description = "Define classes for environment for each product scope.  A product scope refers to each instance of product created by the Product Factory Module. Classes: editor, owner, viewer"
  type = bool
  default = true
}

variable "ENABLE_ORG" {
  description = "The org policy grants classes that can modify any /org/* resources.  This should only be granted to Vault tokens which manage the entire organisation which is managed by the Terraform workspace is-org. Classes: owner"
  type = bool
  default = true
}


variable "ENABLE_OIDC_DEFAULT" {
  description = "The team policy grants a viewer class that allows GCP users to auth and use glob This should only be granted to Vault tokens which manage the entire organisation which is managed by the Terraform workspace is-org. Classes: viewer"
  type = bool
  default = false
}

variable "ENABLE_DB_INSTANCE" {
  description = "The db-instance-engine policy allows the token holder to create new db secret engine mounts and rotate the root secret on each DB instance.  Product owners can manage specific database credentials. Classes: owner"
  type = bool
  default = false
}