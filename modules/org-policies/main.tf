/* Specific policies should be managed by a .tf file that has a single scope,
 * for example: org, product, token, db-instance. Documentation for each policy
 * should be included in the variables.tf file and describe the classes they
 * support.
 *
 * Only put code here that is global to policy management.
 */