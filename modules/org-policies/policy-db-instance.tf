data "vault_policy_document" "policy_doc_db_instance_owner" {
  count = var.ENABLE_DB_INSTANCE ? 1 : 0
  rule {
    path = "/sys/mounts"
    capabilities = ["read"]
  }
  rule {
    path = "/sys/mounts/org/db/*"
    capabilities = ["read", "create", "update", "delete"]
  }
  rule {
    path = "/org/db/*"
    capabilities = ["read", "create", "update", "delete"]
  }
}

resource "vault_policy" "db_instance_owner" {
  count = var.ENABLE_DB_INSTANCE ? 1 : 0
  name = "db-instance-owner"
  policy = "${data.vault_policy_document.policy_doc_db_instance_owner[count.index].hcl}"
}
