data "vault_policy_document" "policy_doc_token_creator" {
  count = var.ENABLE_TOKEN ? 1 : 0
  rule {
    path         = "/auth/token/create"
    capabilities = ["update"]
  }
}

resource "vault_policy" "token_creator" {
  count = var.ENABLE_TOKEN ? 1 : 0
  name = "token-creator"
  policy = "${data.vault_policy_document.policy_doc_token_creator[count.index].hcl}"
}