module "sql-db_postgresql" {
  source  = "GoogleCloudPlatform/sql-db/google//modules/postgresql"
  version = "2.0.0"
  database_version = var.VERSION
  project_id = var.PROJECT
  zone = "a"
  tier = var.TIER
  name = "${var.NAME}"
  user_name = "postgres"
  backup_configuration = {
    enabled = false
    binary_log_enabled = false
    start_time = "00:00"
  }
  additional_databases = [ for dbname in var.DATABASES : {
        name = dbname
        charset = ""
        collation = ""
        instance = ""
        project = var.PROJECT
  }]
}

resource "vault_mount" "db_secret_engine" {
  path        = "org/db/${var.NAME}"
  type        = "database"
}

resource "vault_database_secret_backend_connection" "db_secret_connection" {
  backend       = vault_mount.db_secret_engine.path
  name          = "default"
  verify_connection = false
  allowed_roles = [ "*" ]

  data = {
    username = "postgres"
    password = module.sql-db_postgresql.generated_user_password
  }

  postgresql {
    connection_url = "postgres:///default?host=/cloudsql/${module.sql-db_postgresql.instance_connection_name}&user={{username}}&password={{password}}"
  }
}

resource "vault_database_secret_backend_role" "role" {
  count               = length(var.DATABASES)
  backend             = vault_mount.db_secret_engine.path
  name                = var.DATABASES[count.index]
  db_name             = "default"
  creation_statements = ["CREATE ROLE \"{{name}}\" WITH LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}';"]
  depends_on          = [ "vault_database_secret_backend_connection.db_secret_connection" ]
  default_ttl         = var.DEFAULT_TTL
  max_ttl             = var.MAX_TTL
}