/********************************************************
 * Required
 ********************************************************/

variable "PROJECT" {
  description = "ID of project that will host the new database instance"
  type = string
}

variable "NAME" {
  description = "Name of the database instance being created - letters/numbers/dashes - this will make up part of a url so normal domain name rules apply."
  type = string
}

variable "DATABASES" {
  description = "List of databases to include in this instance"
  type = list
}

/********************************************************
 * Optional
 ********************************************************/

variable "TIER" {
  description = "Cloud SQL Tier"
  type = string
  default = "db-f1-micro"
}

variable "VERSION" {
  description = "Database version"
  type = string
  default = "POSTGRES_9_6"
}

variable "DEFAULT_TTL" {
  description = "Default length of time in seconds that the role is valid for"
  type = number
  default = 120
}

variable "MAX_TTL" {
  description = "Max length of time in seconds that the role is valid for"
  type = number
  default = 300
}