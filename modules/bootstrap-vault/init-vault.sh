#!/bin/bash

#######################################################################################################
# Setup initial configuration of Vault to be used to managed project secrets in GCP
#
#######################################################################################################

echo "Running as GCP account: $PROJECT_USER"

vault_creds=~/.config/gcloud/vault-gcp-secret-engine.json
vault_service_account="Vault GCP Secret Engine Service Account"

echo -n "Enter your external Vault Address URI (ex https://vault.myorg.com): "
read VAULT_ADDR

echo -n "Enter your OIDC Client ID: "
read oidc_client_id

echo -n "Enter your OIDC Client Secret: "
read -s oidc_client_secret

export VAULT_ADDR oidc_client_id oidc_client_secret

# Initialize Vault
vault operator init

# First login using gcloud
gcloud auth login $PROJECT_USER

admin_project_id=`gcloud projects list --filter="name=${PROJECT_NAME}" --format='value(PROJECT_ID)'`
org_id=`gcloud organizations list --limit=1 --format='value(ID)'`
google_auth_plugin_sha_sum=644fa8acbd704169abf3317de603577744f7f212825b0fae7c6ad8295eed5969


# Create the Vault SA Service account if it doesn't already exist
content=`gcloud iam service-accounts list --filter="$vault_service_account" --uri 2>/dev/null | awk '{n=split($0,a,"/"); print a[n] }'`
if [ -z $content ]; then
  gcloud iam service-accounts create vault-secret-engine --display-name "$vault_service_account"
  gcloud iam service-accounts keys create ${vault_creds} --iam-account vault-secret-engine@${admin_project_id}.iam.gserviceaccount.com
fi

# Add permissions for the Vault Service account to organization
gcloud organizations add-iam-policy-binding $org_id --member serviceAccount:vault-secret-engine@${admin_project_id}.iam.gserviceaccount.com --role roles/iam.serviceAccountAdmin
gcloud organizations add-iam-policy-binding $org_id --member serviceAccount:vault-secret-engine@${admin_project_id}.iam.gserviceaccount.com --role roles/iam.serviceAccountKeyAdmin
gcloud organizations add-iam-policy-binding $org_id --member serviceAccount:vault-secret-engine@${admin_project_id}.iam.gserviceaccount.com --role roles/resourcemanager.projectIamAdmin
gcloud organizations add-iam-policy-binding $org_id --member serviceAccount:vault-secret-engine@${admin_project_id}.iam.gserviceaccount.com --role roles/resourcemanager.organizationAdmin

echo "Use your root token to login to vault"
vault login

vault secrets enable -path=org/gcp gcp
vault write org/gcp/config credentials=@$vault_creds

vault write org/gcp/roleset/provisioner \
    project="$admin_project_id" \
    secret_type="access_token"  \
    token_scopes="https://www.googleapis.com/auth/cloud-platform" \
    bindings=-<<EOF
      resource "//cloudresourcemanager.googleapis.com/organizations/$org_id" {
        roles = ["roles/resourcemanager.folderCreator", "roles/resourcemanager.projectCreator", "roles/viewer", "roles/storage.admin", "roles/resourcemanager.organizationAdmin", "roles/billing.admin", "roles/iam.serviceAccountAdmin", "roles/run.admin", "roles/serviceusage.serviceUsageAdmin"]
      }
EOF
