# Manage auth backends broadly across Vault
path "auth/*"
{
  capabilities = ["create", "read", "update", "delete", "list"]
}

path "sys/auth"
{
  capabilities = ["read"]
}

# List, create, update, and delete auth backends
path "sys/auth/*"
{
  capabilities = ["create", "read", "update", "delete"]
}

# List existing policies
path "sys/policy"
{
  capabilities = ["read"]
}

# Create and manage policies
path "sys/policy/*"
{
  capabilities = ["create", "read", "update", "delete", "list"]
}

# Create and manage ACL policies
path "sys/policies/acl"
{
  capabilities = ["read"]
}

# Create and manage ACL policies
path "sys/policies/acl/*"
{
  capabilities = ["create", "read", "update", "delete", "list"]
}

# List, create, update, and delete org secrets
path "org/*"
{
  capabilities = ["create", "read", "update", "delete", "list"]
}

# List, create, update, and delete product secrets
path "product/*"
{
  capabilities = ["create", "read", "update", "delete", "list"]
}

# Manage secret backends
path "sys/mounts"
{
  capabilities = ["read"]
}
path "sys/mounts/*"
{
  capabilities = ["create", "read", "update", "delete", "list"]
}