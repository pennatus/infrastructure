# Manage auth backends broadly across Vault
# Allow admin the ability to manage identites - i.e. groups
path "identity/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "auth/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "sys/auth"
{
  capabilities = ["create", "read", "update", "delete", "list"]

}

# List, create, update, and delete auth backends
path "sys/auth/*"
{
  capabilities = ["create", "read", "update", "delete", "sudo"]
}
# Used to move an existing secret engine mount point
path "sys/remount" 
{
  capabilities = ["update", "sudo"]
}


# List existing policies
path "sys/policy"
{
  capabilities = ["read"]
}

# Create and manage policies
path "sys/policy/*"
{
  capabilities = ["create", "read", "update", "delete", "list"]
}

# View ACL policies
path "sys/policies/acl"
{
  capabilities = ["list", "read"]
}

# Create and manage ACL policies
path "sys/policies/acl/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List, create, update, and delete org secrets
path "org/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List, create, update, and delete product secrets
path "product/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Manage secret backends
path "sys/mounts"
{
  capabilities = ["read"]
}
# Manage and manage secret backends broadly across Vault.
path "sys/mounts/*"
{
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Read health checks
path "sys/health"
{
  capabilities = ["read", "sudo"]
}
