data "google_organization" "org" {
  domain = var.DOMAIN
}

data "google_billing_account" "acct" {
  display_name = var.BILLING_DISPLAY_NAME
}

locals {
  env_project_map = { for env in var.ENVIRONMENTS :
    env => {
      id = google_project.project[index(var.ENVIRONMENTS, env)].project_id
      number = google_project.project[index(var.ENVIRONMENTS, env)].number
    }
  }
  env_api_list = flatten([
    for env in var.ENVIRONMENTS : [
      for api in var.APIS : {
        env = env
        api = api
      }
    ]
  ])
  env_ssh_list = flatten([
    for env in var.ENVIRONMENTS : [
      for ssh_key in var.SSH_KEYS : {
        env = env
        ssh_key = ssh_key
      }
    ]
  ])
  env_kv_list = flatten([
    for env in var.ENVIRONMENTS : [
      for kv_path in var.KV_SECRET_PATHS : {
        env = env
        kv_path = kv_path
      }
    ]
  ])
}

resource "random_string" "project_id_suffix" {
  count = length(var.ENVIRONMENTS)
  length = 22
  upper = false
  special = false
}

resource "google_project" "project" {
  count = length(var.ENVIRONMENTS)
  name = "${var.ENVIRONMENTS[count.index]}-${var.PRODUCT_NAME}"
  project_id = "${substr(var.ENVIRONMENTS[count.index], 0, 3)}-${substr(var.PRODUCT_NAME, 0, 20)}-${substr(random_string.project_id_suffix[count.index].result, 0, 5)}"
  folder_id  = var.FOLDER_IDS[var.ENVIRONMENTS[count.index]]
  billing_account = data.google_billing_account.acct.id
}

resource "google_project_service" "iamapi" {
  count = length(var.ENVIRONMENTS)
  project = google_project.project[count.index].project_id
  service = "iam.googleapis.com"
}

resource "google_project_service" "compute" {
  count = length(var.ENVIRONMENTS)
  project = google_project.project[count.index].project_id
  service = "compute.googleapis.com"
}

resource "google_project_service" "serviceusageapi" {
  count = length(var.ENVIRONMENTS)
  project = google_project.project[count.index].project_id
  service = "serviceusage.googleapis.com"
}

resource "google_project_service" "cloudresourcemanagerapi" {
  count = length(var.ENVIRONMENTS)
  project = google_project.project[count.index].project_id
  service = "cloudresourcemanager.googleapis.com"
}

resource "google_project_service" "api" {
  count = length(local.env_api_list)
  project = local.env_project_map[local.env_api_list[count.index].env].id
  service = "${local.env_api_list[count.index].api}.googleapis.com"
}

resource "google_compute_project_metadata" "ssh_key" {
  count = length(local.env_ssh_list)
  project = local.env_project_map[local.env_ssh_list[count.index].env].id
  metadata = {
      ssh-keys = local.env_ssh_list[count.index].ssh_key
  }
  depends_on = [ "google_project.project", "google_project_service.compute" ]
}

resource "vault_mount" "prod_secret_kv_2" {
  count = length(local.env_kv_list)
  path        = "product/${local.env_kv_list[count.index].env}/${var.PRODUCT_NAME}/kv-v2/${local.env_kv_list[count.index].kv_path}"
  type        = "kv-v2"
}

resource "vault_gcp_secret_roleset" "gcp_backend_secret_owner" {
  count = length(var.ENVIRONMENTS)
  backend      = "/org/gcp"
  roleset      = "${substr(var.ENVIRONMENTS[count.index], 0, 3)}-${substr(var.PRODUCT_NAME, 0, 7)}-ow"
  secret_type  = "access_token"
  project      = google_project.project[count.index].project_id
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${google_project.project[count.index].project_id}"

    roles = [
      "roles/owner",
    ]
  }

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${var.ADMIN_PROJECT}"

    roles = [
      "roles/monitoring.notificationChannelEditor",
      "roles/monitoring.alertPolicyEditor",
      "roles/monitoring.uptimeCheckConfigEditor",
    ]
  }

  depends_on = [ "google_project_service.iamapi", "google_project_service.compute" ]
}

resource "vault_gcp_secret_roleset" "gcp_backend_secret_editor" {
  count = length(var.ENVIRONMENTS)
  backend      = "/org/gcp"
  roleset      = "${substr(var.ENVIRONMENTS[count.index], 0, 3)}-${substr(var.PRODUCT_NAME, 0, 7)}-ed"
  secret_type  = "access_token"
  project      = google_project.project[count.index].project_id
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${google_project.project[count.index].project_id}"

    roles = [
      "roles/editor",
    ]
  }

  depends_on = [ "google_project_service.iamapi", "google_project_service.compute" ]
}

resource "vault_gcp_secret_roleset" "gcp_backend_secret_viewer" {
  count = length(var.ENVIRONMENTS)
  backend      = "/org/gcp"
  roleset      = "${substr(var.ENVIRONMENTS[count.index], 0, 3)}-${substr(var.PRODUCT_NAME, 0, 7)}-vi"
  secret_type  = "access_token"
  project      = google_project.project[count.index].project_id
  token_scopes = ["https://www.googleapis.com/auth/cloud-platform"]

  binding {
    resource = "//cloudresourcemanager.googleapis.com/projects/${google_project.project[count.index].project_id}"

    roles = [
      "roles/viewer",
    ]
  }

  depends_on = [ "google_project_service.iamapi", "google_project_service.compute" ]
}


data "vault_policy_document" "policy_doc_viewer" {
  count = length(var.ENVIRONMENTS)
  rule {
    path         = "/product/${var.ENVIRONMENTS[count.index]}/${var.PRODUCT_NAME}/*"
    capabilities = ["read"]
  }
  rule {
    path         = "/org/gcp/token/${substr(var.ENVIRONMENTS[count.index], 0, 3)}-${substr(var.PRODUCT_NAME, 0, 7)}-vi"
    capabilities = ["read"]
  }
}

data "vault_policy_document" "policy_doc_editor" {
  count = length(var.ENVIRONMENTS)
  rule {
    path         = "/product/${var.ENVIRONMENTS[count.index]}/${var.PRODUCT_NAME}/*"
    capabilities = ["read", "update", "list"]
  }
  rule {
    path         = "/org/gcp/token/${substr(var.ENVIRONMENTS[count.index], 0, 3)}-${substr(var.PRODUCT_NAME, 0, 7)}-vi"
    capabilities = ["read"]
  }
  rule {
    path         = "/org/gcp/token/${substr(var.ENVIRONMENTS[count.index], 0, 3)}-${substr(var.PRODUCT_NAME, 0, 7)}-ed"
    capabilities = ["read"]
  }
}

data "vault_policy_document" "policy_doc_owner" {
  count = length(var.ENVIRONMENTS)
  rule {
    path         = "/product/${var.ENVIRONMENTS[count.index]}/${var.PRODUCT_NAME}/*"
    capabilities = ["create", "read", "update", "delete", "list"]
  }
  rule {
    path         = "/org/gcp/token/${substr(var.ENVIRONMENTS[count.index], 0, 3)}-${substr(var.PRODUCT_NAME, 0, 7)}-*"
    capabilities = ["read"]
  }
}

resource "vault_policy" "viewer" {
  count = length(var.ENVIRONMENTS)
  name   = "product-${var.ENVIRONMENTS[count.index]}-${var.PRODUCT_NAME}-viewer"
  policy = "${data.vault_policy_document.policy_doc_viewer[count.index].hcl}"
}

resource "vault_policy" "editor" {
  count = length(var.ENVIRONMENTS)
  name   = "product-${var.ENVIRONMENTS[count.index]}-${var.PRODUCT_NAME}-editor"
  policy = "${data.vault_policy_document.policy_doc_editor[count.index].hcl}"
}

resource "vault_policy" "owner" {
  count = length(var.ENVIRONMENTS)
  name   = "product-${var.ENVIRONMENTS[count.index]}-${var.PRODUCT_NAME}-owner"
  policy = "${data.vault_policy_document.policy_doc_owner[count.index].hcl}"
}

resource "vault_gcp_auth_backend_role" "gcp" {
  count = length(var.ENVIRONMENTS)
  role                   = "product-${var.ENVIRONMENTS[count.index]}-${var.PRODUCT_NAME}-viewer"
  type                   = "iam"
  backend                = "gcp"
  bound_projects         = [ google_project.project[count.index].project_id ]
  bound_service_accounts = ["${google_project.project[count.index].number}-compute@developer.gserviceaccount.com"]
  token_policies         = ["product-${var.ENVIRONMENTS[count.index]}-${var.PRODUCT_NAME}-viewer"]
}
