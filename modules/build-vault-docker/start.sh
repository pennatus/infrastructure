#!/bin/sh
export SKIP_SETCAP=1

/usr/local/bin/tinyproxy-bootstrap.sh &

exec /usr/local/bin/docker-entrypoint.sh server
