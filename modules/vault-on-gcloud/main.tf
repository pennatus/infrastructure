# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY A VAULT INSTANCE IN GOOGLE CLOUD RUN
# ---------------------------------------------------------------------------------------------------------------------

data "google_organization" "org" {
  domain = var.DOMAIN
}

data "google_project" "admin_project" {
  project_id = var.ADMIN_PROJECT
}

# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY THE VAULT INSTANCE
# ---------------------------------------------------------------------------------------------------------------------

resource "random_uuid" "keyring_uuid" { }
resource "random_uuid" "storage_uuid" { }
resource "random_uuid" "serviceaccount_uuid" { }

resource "google_service_account" "vault_serviceaccount" {
  account_id   = "vault-sa-${substr(random_uuid.storage_uuid.result, 0, 20)}"
  project = var.ADMIN_PROJECT
  display_name = "Vault Cloud Run Service Account"
}

locals {
  project_roles = [ "roles/storage.admin", "roles/cloudkms.cryptoKeyEncrypterDecrypter", "roles/cloudkms.admin" ]
  organization_roles = [ "roles/iam.serviceAccountAdmin", "roles/iam.serviceAccountKeyAdmin", "roles/resourcemanager.projectIamAdmin", "roles/resourcemanager.organizationAdmin", "roles/cloudsql.client" ]
}

resource "google_project_iam_member" "project_member_roles" {
  count = length(local.project_roles)
  project = var.ADMIN_PROJECT
  role    = local.project_roles[count.index]

  member = "serviceAccount:${google_service_account.vault_serviceaccount.email}"
}

resource "google_organization_iam_member" "organization_member_roles" {
  count = length(local.organization_roles)
  org_id  = data.google_organization.org.id
  role    = local.organization_roles[count.index]

  member = "serviceAccount:${google_service_account.vault_serviceaccount.email}"
}

resource "google_storage_bucket" "vault_backend_storage" {
  name     = "vault-storage-${random_uuid.storage_uuid.result}"
  project = var.ADMIN_PROJECT
  location = "us-central1"
  force_destroy = "true"
}

resource "google_kms_key_ring" "vault_key_ring" {
  name = "vault-keyring-${random_uuid.keyring_uuid.result}"
  project = var.ADMIN_PROJECT
  location = "global"
  depends_on = [ google_project_service.kmsapi ]
}

resource "google_kms_crypto_key" "vault_key" {
  name            = "vault-key"
  key_ring        = google_kms_key_ring.vault_key_ring.self_link
  rotation_period = "100000s"

  lifecycle {
    prevent_destroy = false
  }
}

locals {
  sql_instances = join(",", var.SQL_INSTANCES)
}

resource "google_cloud_run_service" "vault" {
  name     = var.VAULT_SERVICE_NAME
  provider = google-beta
  location = "us-central1"

  metadata {
    namespace = var.ADMIN_PROJECT
  }

  spec_metadata {
    annotations = {
      "run.googleapis.com/cloudsql-instances" = local.sql_instances
    }
  }

  project = var.ADMIN_PROJECT

  spec {
    service_account_name = google_service_account.vault_serviceaccount.email
    containers {
      image = "gcr.io/pennatus-images/vault-docker:${var.VAULT_VERSION}"
      env {
        name = "VAULT_LOCAL_CONFIG"
        value = "{\"ui\": \"true\", \"plugin_directory\": \"/etc/vault/vault_plugins\", \"listener\": [{\"tcp\": {\"address\": \"0.0.0.0:7200\", \"tls_disable\": 1}}], \"backend\": {\"gcs\": {\"bucket\": \"vault-storage-${random_uuid.storage_uuid.result}\"}}, \"seal\": {\"gcpckms\": {\"region\": \"global\", \"project\": \"${var.ADMIN_PROJECT}\", \"key_ring\": \"vault-keyring-${random_uuid.keyring_uuid.result}\", \"crypto_key\": \"vault-key\"}"
      }
    }
  }

  # Use to set the IAM bindings to allow public access since the provider doesn't give
  # us another way yet.
  provisioner "local-exec" {
    command = "curl -X POST -d '{\"policy\": {\"bindings\":[{\"role\": \"roles/run.invoker\",\"members\": [\"allUsers\"]}]}}' -H \"Content-Type: application/json\" -H \"Authorization: Bearer ${var.GCP_ACCESS_TOKEN}\" https://us-central1-run.googleapis.com/v1/projects/${var.ADMIN_PROJECT}/locations/us-central1/services/vault:setIamPolicy"
  }

  depends_on = [google_project_service.runapi, google_storage_bucket.vault_backend_storage, google_kms_crypto_key.vault_key]
}

resource "google_project_service" "resourcemanagerapi" {
  project = var.ADMIN_PROJECT
  service = "cloudresourcemanager.googleapis.com"
}

# Used for making connections to databases
resource "google_project_service" "sqladminapi" {
  project = var.ADMIN_PROJECT
  service = "sqladmin.googleapis.com"
}

resource "google_project_service" "runapi" {
  # This is a bit ugly but it seems like if this is the first time enabling runapi then
  # it can take some time for the permissions to propogate through the network.  I haven't
  # found a better way to do this.

  project = var.ADMIN_PROJECT
  service = "run.googleapis.com"
  disable_on_destroy = false
  depends_on = [ google_project_service.resourcemanagerapi ]

  provisioner "local-exec" {
    command = "sleep 60"
  }
}

resource "google_project_service" "kmsapi" {
  project = var.ADMIN_PROJECT
  service = "cloudkms.googleapis.com"
  depends_on = [ google_project_service.resourcemanagerapi ]
}

resource "google_project_service" "iamcredsapi" {
  project = var.ADMIN_PROJECT
  service = "iamcredentials.googleapis.com"
  disable_dependent_services = true
  depends_on = [ google_project_service.resourcemanagerapi ]
}

# The iamapi is needed to create the project owner vault service accounts and
# any other service account vault needs to create.
resource "google_project_service" "iamapi" {
  project = var.ADMIN_PROJECT
  service = "iam.googleapis.com"
  depends_on = [ google_project_service.resourcemanagerapi ]
}

resource "google_project_service" "monitoringapi" {
  project = var.ADMIN_PROJECT
  service = "monitoring.googleapis.com"
  disable_dependent_services = true
  disable_on_destroy = false
  depends_on = [ google_project_service.resourcemanagerapi ]
}