# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# These parameters must be supplied.
# ---------------------------------------------------------------------------------------------------------------------

variable "ADMIN_PROJECT" {
  description = "This is the infrastructure project to deploy vault under - project id"
}

variable "GCP_ACCESS_TOKEN" {
  description = "Short lived GCP access token.  Get using `gcloud auth print-access-token`"
}

variable "DOMAIN" {
  description = "Domain of the organization"
}


# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# ---------------------------------------------------------------------------------------------------------------------

variable "VAULT_VERSION" {
  description = "This is the docker tagged version of Vault"
  default     = "latest"
}

variable "SQL_INSTANCES" {
  description = "List of SQL instances to add connections to"
  type = list
  default = [ ]
}

variable "VAULT_SERVICE_NAME" {
  description = "Name of Vault service as it appears in Cloud Run"
  type = string
  default = "vault"
}