# The Service is ready to be used when the "Ready" condition is True
# Due to Terraform and API limitations this is best accessed through a local variable
# locals {
#   cloud_run_status = {
#     for cond in google_cloud_run_service.vault.status.conditions :
#     cond.type => cond.status
#   }
# }

# output "ready" {
#   value = local.cloud_run_status["Ready"] == "True"
# }

output "url" {
  value = google_cloud_run_service.vault.status[0].url

  depends_on = [ null_resource.delay ]
}

