#!/bin/bash

#######################################################################################################
# Setup initial infrastructure on GCP.
#
# At the end of the script you will have the following:
#  * The user you authenticate with will have the `roles/resourcemanager.folderAdmin` role added to it.
#  * A new project under `Shared Infrastructure` will be created called `terraform-$sld` where
#    $sld is the second level domain for your organization - i.e. mydomain for mydomain.com
#  * A new service account will be created under this project called `Terraform Admin Account`.  This
#    will assigned the following roles on the Organization:
#      roles/resourcemanager.projectCreator
#      roles/run.admin
#      roles/iam.serviceAccountUser
#  * Some GCP cloud APIs will be enabled:
#      cloudresourcemanager.googleapis.com
#      iam.googleapis.com
#  * A storage bucket for the `tf.state` will be created under the `terraform-$sld` project.
#######################################################################################################
echo "Running as GCP account: $PROJECT_USER"

vault_creds=~/.config/gcloud/vault-admin.json

# First login using gcloud
gcloud auth login $PROJECT_USER
gcloud config set account $PROJECT_USER

admin_project_id=`gcloud projects list --filter="name=${PROJECT_NAME}" --format='value(PROJECT_ID)'`
domain=`gcloud organizations list --limit=1 --format='value(DISPLAY_NAME)'`
org_id=`gcloud organizations list --limit=1 --format='value(ID)'`
billing_id=`gcloud beta billing accounts list --limit=1 --format='value(ACCOUNT_ID)'`
if [ -z $billing_id ]; then
  echo "No billing account associated with the account.  Make sure you have a billing account and that the user you authenticated with is at least a `Billing Account User` in the organization."
  exit 1
fi

vault_service_account="Vault Admin Account"

# Create the Admin project if it doesn't already exist
if [ -z $admin_project_id ]; then
  admin_project_id=${PROJECT_NAME:0:11}-`uuidgen | head -c${1:-18} | tr '[:upper:]' '[:lower:]'`
  gcloud projects create $admin_project_id --name=$PROJECT_NAME --organization $org_id --set-as-default
  admin_project_id=`gcloud config list --format 'value(core.project)'`
else
  gcloud config set project ${admin_project_id}
fi

gcloud beta billing projects link ${admin_project_id} --billing-account $billing_id

# Create the Vault Admin Service account if it doesn't already exist
content=`gcloud iam service-accounts list --filter="$vault_service_account" --uri 2>/dev/null | awk '{n=split($0,a,"/"); print a[n] }'`
if [ -z $content ]; then
  gcloud iam service-accounts create vault-admin --display-name "$vault_service_account"
  gcloud iam service-accounts keys create ${vault_creds} --iam-account vault-admin@${admin_project_id}.iam.gserviceaccount.com
fi

# Add permissions for the Vault Service account to project
gcloud projects add-iam-policy-binding $admin_project_id --member serviceAccount:vault-admin@${admin_project_id}.iam.gserviceaccount.com --role roles/storage.admin
gcloud alpha projects add-iam-policy-binding $admin_project_id --member serviceAccount:vault-admin@${admin_project_id}.iam.gserviceaccount.com --role roles/cloudkms.cryptoKeyEncrypterDecrypter

# Enable service so we can push vault docker image
gcloud services enable containerregistry.googleapis.com --project $admin_project_id
gcloud services enable cloudresourcemanager.googleapis.com --project $admin_project_id

# Enable gcloud as an auth provider for docker login
gcloud auth configure-docker

cat << EOF > $BUILD_DIR/project.auto.tfvars
ADMIN_PROJECT = "$admin_project_id"
DOMAIN = "$domain"
EOF
