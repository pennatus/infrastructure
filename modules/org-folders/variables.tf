/********************************************************
 * Required but set from the auto.tfvars
 ********************************************************/

variable "DOMAIN" {
  description = "Domain that is hosting the account"
  type = string
}

/********************************************************
 * Optional
 ********************************************************/

variable "ENVIRONMENTS" {
  description = "List of environment that apply for this product"
  type = list
  default = [ "dev", "staging", "prod" ]
}

