# org-folders

Creates a list of folders for an organisation

## Usage

```hcl

module "org_folders" {
  source = "git::https://gitlab.com/pennatus/infrastructure//modules/org-folders"

  DOMAIN = var.DOMAIN
  ENVIRONMENTS = var.ENVIRONMENTS
}

```


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| DOMAIN | Domain that is hosting the account | string | n/a | yes |
| ENVIRONMENTS | List of environment that apply for this product | list | `[ "dev", "staging", "prod" ]` | no |

## Outputs

| Name | Description |
|------|-------------|
| folder\_ids |  |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

