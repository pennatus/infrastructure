# Create our main top level folders.
data "google_organization" "org" {
  domain = var.DOMAIN
}

module "folders" {
  source  = "terraform-google-modules/folders/google"
  version = "~> 2.0"

  parent  = "${data.google_organization.org.name}"

  names = var.ENVIRONMENTS
}