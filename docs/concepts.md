
# Structural Concepts

When working at the organizational level it is hard to keep track of the terminology because many terms overlap, for example GCP uses "Projects" as does Gitlab, but then we all understand the general concept of a project.

## Pillars of Design

There are many fundamental patterns, methodologies and practices that developers use to build with.  This project uses the following pillars:

- 12-factor apps (Secrets as ENV)
- Immutable Infrastructure (Dockerfile, Ansible+Packer, Cloudinit, etc. to create versioned images)
- Service Config as Code (ex: Using TF to configure roles in Vault)
- CI/CD for Infrastructure/Config/Code  (Everything should be a Commit/Review/Merge process)
- Dynamic Config as Code (ex: Using Ansible to rollout a Security Patch)
- Release Versions as Code (ex: Creating source controlled "lock" files for published versions )
- Product as Code

## Definitions

- **Products** are composed of Workspaces, Projects, Artifacts and Repositories.  They should be capable (but not required) to be fully managed by separate teams.
- **Projects** are a specific implementation of a Product Environment which contain Resources (managed by Workspaces)
- **Environments** imply security policies (GCP Folders), access (GCP Folders), data permanence (ex: production DB data) and resource architecture
- **Organizational or Shared Infrastructure** are global resources in Production Projects useable by any Project and versioned indepent of the Products that use them (Service Config as Code goes here too)
- **Project Infrastructure** is versioned (source control) resources managed by Terraform Workspaces with state independent of Shared Infrastructure and other Products. (ex: lillooet-prod may have a performant database server resource)
- **Repositories** contain source code and build code that produce versioned Artifacts that can be pushed to registries and deployed as Release to a Project.
- **Releases** are Artifacts that have been deployed to a project.

```mermaid
graph TD
    d1 --> e1
    d2 --> e2
    d3 --> e3
    d4 --> e4
    d5 --> e5
    d6 --> e6
    d7 --> e7
    g1 --> c3
    g1 --> c2
    g1 --> c4
    g2 --> c1
    c1 --> d1
    c1 --> d2
    c2 --> d3
    c2 --> d4
    c2 --> d5
    c3 --> d6
    c4 --> d7
    c2 --> b1
    c2 --> b2
    c4 --> b3
    e1 <--> a1
    e2 <--> a1
    e3 <--> a1
    e4 <--> a1
    e5 <--> a1
    e6 <--> a1
    e7 <--> a1

    subgraph Product Artifacts
    b1[Widget-Docker-v1]
    b2[Widget-Docker-v2]
    b3[Wodgit-ComputeImage-v1]
    end
    subgraph Gitlab Groups
    g1[myorg]
    g2[myorg/infrastructure]
    end
    subgraph Gitlab Repositories
    c1[Infrastructure]
    c2[Widget]
    c3[Wodgit]
    c4[Wadgot]
    end
    subgraph Terraform Workspaces
    d1[IS-prod]
    d2[IS-staging]
    d3[Widget-prod]
    d4[Widget-staging]
    d5[Widget-dev]
    d6[Wodgit-prod]
    d7[Wadgot-prod]
    end
    subgraph GCP Organization
    a1[myorg.com]
    end
    subgraph Products
    f2[Infrastructure]
    f3[Widget]
    f4[Wodgit]
    f5[Wadgot]
    end
    subgraph GCP Projects
    e1((dev-Widget))
    e2((stg-Widget))
    e3((prd-Widget))
    e4((prd-IS))
    e5((stg-IS))
    e6((prd-Wodget))
    e7((prd-Wadgot))
    end

    class a1 className;
    classDef className fill:#f9f,stroke:#333;
```

## Workflows

If we use the Pillars of Design we can map management components and stages of each product piplines to workflows that are collaborative both within a team and accross teams:

- Project Management -> Plan/Work/Release
- Code Management -> Commit/Review/Merge
- Infrastructure Management -> Plan/Review/Apply
- Release Version Management - > Commit/Review/Merge
- Environment Management -> Plan/Review/Apply
- Service Config Management -> Commit/Review/Merge

This decoupling allows an engineering team to propose infrastructure changes that have to be reviewed by the security and infrastructure team before being applied.

### Product Management

Products can be managed by several teams or a single team.  For example, DevOps could be responsible for all production Infrastructure as Code and a Product Team may just own their staging/dev resources.  Or Product A team may be responsible for maintaining their own production IaC.  The key to allowing this to happen is assigning gates in either the Commit/Review/Merge workflow or the Plan/Review/Apply, or both.

**DevOps Deploys, Product Team Implements**: In this model, the product team could implement IaC changes, merge them and the DevOps team can review the plan and choose whether to apply.  They could deny the request based on security concerns or other areas that the DevOps team is skilled in.  In the case both teams have access to the IaC repo, but only the DevOps team has access to the Terraform workspaces.

**DevOps for Prod, Product Team for Staging/Dev**: In this model code commits from the Product team are not allowed on prod and is enforced by Gitlab approval rules as well as Workspace membership in Terraform.io.  However the staging/dev directories are unprotected and the product team has access to to those Workspaces.

In all cases, there should be a combination of privileges either through Gitlab membership (group or project), Gitlab approval processes and Terraform access privileges.

## Patterns

### Terraform modules

Terraform modules are key to testable "battle hardeneding" infrastructure.  Generally a module is required if:

- DRY principles apply
- Resources require `depends_on` keyword
- Multiple resources are need to create a single usable unit

Examples:

- Shared Postgres server
- Networking
- Projects

### Terraform examples

Examples can be used to create test cases for Terraform modules.  They also serve as documentation by showing usage.  New example versions should be created anytime a module introduces a new feature or a breaking change so that consumers of modules can be prepared before updating their plans to include new module versions.
