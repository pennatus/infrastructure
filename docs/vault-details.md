# Vault Configuration and Policies

Vault uses secret backends to protect regular key/value type secrets and to issue authentication tokens (GCP service accounts/tokens, SSH keys, certificates for PKI, Vault tokens) that identify the particular user.  Once a user has been identified, the policies allocated to that user, or group the user belongs to, are applied to indicate what the user is authorized to do.

### Configuration created during Vault bootstrap

* /org/gcp/vault-sa

### Configuration created during product creation

The following will be created in the product factory module.  The `collection` in the `kv-2` secret engine is optional and dependent on the product requirements.

* /product/prod/\<product>/gcp/viewer
* /product/prod/\<product>/gcp/editor
* /product/prod/\<product>/gcp/admin
* /product/prod/\<product>/kv-2/\<resource>/\<collection>/key1
* /product/prod/\<product>/kv-2/\<resource>/\<collection>/keyN
* /product/prod/\<product>/ssh/\<rolleset>
* /product/prod/\<product>/pki
* /product/prod/\<product>/etc...

## Vault Policiies

The following polices dictate different level of access to the secret engines configured above.

There will be `viewer`, `editor`, and `admin` versions of each of the policies below.

-`viewer`: [READ]
-`viewer`: [READ,LIST,UPDATE]
-`owner`: [CREATE,READ,UPDATE,DELETE,LIST]

### Policies created during Vault bootstrap

* /org/*

### Policies created during product creation

The `env` below could be `prod`, `staging`, or `development`. The `product` below is the product name, i.e. `website`.

* /product/*
* /product/\<env>/*
* /product/\<env>/\<product>/*
