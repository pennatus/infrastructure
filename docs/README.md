# Organization Infrastructure

What follows is a possible structure to apply to your organization.  When setting up your infrastructure repository, setup your folder layour according to the following diagram, keeping in mind that for a small organization the `Business Units` and `Teams` tiers may be omitted.

```mermaid
graph TD
    a1 --> b1
    a1 --> b2
    a1 --> e4
    b2 --> c1
    b2 --> c2
    c1 --> d1
    c1 --> d2
    c2 --> d3
    c2 --> d4
    c2 --> d5
    d3 --> e1
    d3 --> e2
    d3 --> e3
    subgraph Organization
    a1[myorg.com]
    end
    subgraph Business Units
    b1[BU 1]
    b2[BU 2]
    end
    subgraph Teams
    c1[Marketing]
    c2[Engineering]
    end
    subgraph Env1
    d1[staging]
    d2[prod]
    end
    subgraph Env2
    d3[dev]
    d4[staging]
    d5[prod]
    end
    subgraph Projects
    e1((dev-product1))
    e2((dev-product2))
    e3((dev-product3))
    e4((org-infra))
    end

    class a1 className;
    classDef className fill:#f9f,stroke:#333;
```

## Legend

```mermaid
graph LR
    a1[Organization]
    b1[GCP Folders]
    c1((GCP Projects))

    class a1 className;
    classDef className fill:#f9f,stroke:#333;
```

## Terms and Concepts

An organization is a composition of many concepts with overlapping terminology.  See [Background of Concepts](concepts.md) for detailed description of the organization domain space.

## Decomposition Details

Business units and teams are optional levels.  For a very small organization you may start with just the Env tier below the organization.  Business units are used for major divisions - i.e. when an organization is composed of multiple smaller organizations but you want to keep everything under the same parent organization.

Each product may have a `Prod`, `Staging`, and `Dev` project and it will go under the appropriate team env folder.

Vault will be used to manage authentication and authorization for users (human and server).  Vault can issues Google GCP service account tokens, signed SSH keys, certificates for PKI, and generic tokens.  All of these authentication schems will authorize the user (human and server) to perform different actions on resources in the infrastructure.

See [Vault Configuration/Policies](vault-details.md) for more details.
