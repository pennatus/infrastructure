#!/bin/bash

if [ ! -e .env ]; then
    PROJECT_NAME='is-org'

    echo -n "Enter your full username for the seed account - i.e joe@myorg.com: "
    read PROJECT_USER

    cat << EOF > .env
PROJECT_NAME=$PROJECT_NAME
PROJECT_USER=$PROJECT_USER
BUILD_DIR=${PWD}/example
EOF


fi

. .env

echo "Current project id: ${PROJECT}"
echo "Current project user: ${PROJECT_USER}"
