# Introduction

The intent of this project is that you have an empty GCP organization using either Cloud Identity or G-Suite and would like to manage the entire organization using Vault and Terraform as soon as possible.  This project should take you from an empty organization to one that is managed by Terraform.io and Vault.  At that point it should be possible to extend your organization using the only two work flows: **Code/Review/Merge** and **Plan/Review/Apply**

## Modules

There are two helper modules, `bootstrap-org` and `build-vault-docker` that are used to prepare the organization to be managed using Terraform.  They will be invoked below with `make modules`.

`bootstrap-org` is used to create the initial project and service accounts for Terraform and Vault.  We need these in order to create the "3d printer".  These accounts are used to create everything else moving forward.

`build-vault-docker` is used to build and push the Vault image that will eventually be deployed to Cloud Run.  If we end up using a different module for deploying an enterprise grade Vault then we can skip this step.

`bootstrap-vault` is used to initially configure Vault after it is deployed.  It is invoked below using `make bootstrap-vault`.

The other modules are optional Terraform modules that can be used in your Terraform configuration files for various purposes.

`vault-on-gcloud` is an optional Terraform module that can be declared in your Terraform configuration to deploy a Vault instance on Google Cloud Run.  This is a fully functional Vault instance but is not recommended for a hardened production system.  The recommendation is to use a battle tested Vault module like the one found here [Terraform Google Vault Module](https://github.com/hashicorp/terraform-google-vault)

`org-folders` is an optional Terraform module that is used to setup your initial folder structure in the organization.  See the example for details.

`product-factory` is an optional Terraform module that is used when declaring new products in your organization.  See the example for details.

Finally, `setup-env.sh` can be used to setup your `.env` with your Terraform creds.  These are variables inputs to your Terraform plan if you want to run locally.  You can also grab the value in your `.env` to populate the variable in `terraform.io`.

## Security Model

The purpose of this project is to get you started with Vault and Terraform from day one, therefore this is not considered a final product, but it allows you to iterate your production infrastructure using peer-reviewed deployments.  Therefore, the initial permission model needs to be understood so that it can be later adapted to your security needs.

- Seed Account: A Human, probably the one reading this file
  - Provisions the initial service accounts
  - Creates the initial administration project
  - Associates the necessary roles to the service accounts
- Vault Service Account: Stored in Vault Docker image as JSON file referenced by GOOGLE_ACCOUNT_CREDENTIALS
  - Accesses the storage backend (GS bucket)
  - Unseals the Vault using Google KMS
  - Gives access to the GCP Secret Engine to create GCP Service Accounts and setting IAM policies
  - This service account is only used when deploying to Cloud Run since we need to auto-unseal and we need to use Google as the storage backend.
- Vault Secret Engine Service Account
  - Used to configure the GCP backend that can create access tokens per product and environment at various authorization levels (owner, editor, viewer).
  - Created in the `bootstrap-vault` helper module
- Product Owner Service Account (per Environment):
  - This is used by projects created with the `product-factory` module to grant full access over all resources granted from the organization product seed.  See [docs/concepts.md](docs/concepts.md) for more information on how Products relate to Projects.

## Where to start

This is an opinionated project that makes several assumptions about what you want and how that should be done:

- An empty GCP organization
- Vault will manage all secrets
- Vault is accessible on the Internet
- This is a a stepping stone
- You use Gitlab, Terraform.io and GCP
- Should zero-cost where possible

I'm sure there are variations of this project that might be better, but the idea was to make something that uses Good Practices™ which are hopefully secure, easy and extensible.  As your organization grows more sophisticated solutions can be implemented in a controlled and testable way.

### 1. Create your organization

#### GCP

Disclaimer: Every effort is made to not incur GCP charges, but pricing methodology changes over time and this project could incur charges to your account.

1. Procure a domain name, ex: `awesome-project.com`
1. Create a new Cloud Identity account by going into your personal GMail GCP console -> IAM -> Identities & Organization -> Cloud Identity: click **[Sign Up]**
1. Follow the steps to register your domain with Cloud Identity
1. Make sure you have the following roles (you will likely need to add at least 3 to your account in Org -> IAM):
    1. `Project Creator`
    1. `Billing Account Administrator`
    1. `Folder Admin`
    1. `Organisation Administrator`
1. Link a billing account to this organization (Billing->Add Billing Account)

You now have a new GCP console associated with your domain that has a root Organization and a Seed Account (your account) which allows the creation of Folders and the assignment of additional roles from the root through each folder level.  **Pro Tip: Use Chome profiles to keep your personal and organization login separate to avoid confusing the GCP console**

Before continuing, review this link [default access control](https://cloud.google.com/resource-manager/docs/default-access-control) which explains how to remove the organisation permissions so they don't automatically inherit in all project service accounts.  Otherwise the Product Service Accounts would inherit `Organisation Administrator` giving them access to escalating their own permissions.

Also consider reviewing [using groups and service accounts](https://cloud.google.com/docs/enterprise/best-practices-for-enterprise-organizations#groups-and-service-accounts) to separate duties and assign backups to users using groups instead of directly assigning roles to users.

#### Gitlab.com

If you don't already have a Gitlab account, register for Gitlab

1. Create a group for `awesome-project`
1. Create a repository under `awesome-project` called `infrastructure`
1. Create a directory inside this repo called `awesome-project/infrastructure/prod/is-org`.  Make sure to create the repository with at least a README.md.  This repo must not be empty for the `Terraform.io` steps
1. Sign out and create a new account named `awesome-project-sa`
1. Sign back in as yourself and add the user `awesome-project-sa` to your `infrastructure` subgroup as a `Maintainer` role.
1. Checkout the `infrastructure` repo.  This is where all the Terraform configuration files will go as you build up your vault infrastructure.

You now have a service account that has permissions to only the projects you wish to share with it.  This way you don't have to use your personal account which may have permissions to projects/groups unrelated to `awesome-project`.

#### Terraform.io

If you don't have an account for terraform.io, you can you create a personal account similar to Gitlab.  The structure of terraform.io is that you can be a collaborator to other teams, create multiple organizations and even associate separate repositories to each workspace.  Therefore you should not need a separate account for each organization you work with.

1. Once you've created your account, log-in and create an organization for `awesome-project`.
1. Create a workspace for `is-org`
1. Connect a VCS Provider
    1. Choose gitlab.com as the provider
    1. Log into Gitlab as `awesome-project-sa` to generate the Application key
    1. Follow the instructions to add Terraform as an application to Gitlab.
1. Configure the rest of the workspace options as follows (or however you prefer)
    1. Monitor `/prod/is-org` directory for changes
    1. Monitor `/modules` for changes
    1. Use the default branch

At this point we're very close! You can now check in terraform code to `infrastructure/prod/is-org` and it will be applied to your new GCP organization `awesome-project.com`.  Awesome!

### 2. Initialize your organization

The next step is to create the initial service accounts that terraform.io will use to make changes to your GCP organization and setup your `infrastructure` repository with the initial code from this project.

1. Run `make`.  The `example` subfolder now contains a set of files that will be used to deploy Vault on Cloud Run.
1. Copy all the files in `example` into your `infrastructure/prod/is-org` repository.  Use `cp -a` to copy all files and directories.  Make sure to also copy the `.gitignore` from the `example` folder.
1. Take a moment to review the `variables.tf` file as the folder name choices can not easily be changed (changing could trigger a delete of a folder).
1. Change directory to the new repo `infrastructure/prod/is-org` repository and run the commands below.  When prompted for your GCP key you can enter anything for now:

```console
terraform init
terraform plan
```

1. If the `terraform plan` succeeded then you can checkin and push the repository.
1. Run `gcloud auth print-access-token`.  This will provide you a short lived GCP access token that you can provide Terraform.
1. Login to `terraform.io`.  Since this is your first run you will need to queue the plan manually.  First click on `variables` and add a new `Terraform Variable` called `GCP_ACCESS_TOKEN` and set it to the value from the previous step.  Make sure to mark the variables as `sensitive`.
1. Click `Queue Plan`.  Normally, plans are triggered by a push to the repository but the first plan requires a manual inovocation.
1. After the plan if finished you will need to `Confirm and Apply`.  This triggers the work to begin to create all the resources in the plan.
1. Go into the console in the Cloud Run section and click on your new Vault instance to find out the current URL

### 3. Initialize Vault Configuration

1. Download the Vault CLI agent.
1. Set the environment variable to find your vault: `export VAULT_ADDR=<vault address from terraform state>`

There are a few manual steps that now need to be performed in the Google Console (at the time of writing there were no gcloud commands and the terraform provider was broken):

1. First select the `is-org` project and then click on the `Cloud Run` tool.
1. Click `Manage Custom Domains` and then `Add Mapping`.
1. Select the `Vault` service and then specify a subdomain like `vault.myorg.com`.  Click `Continue`.  You will need to setup some CNAME records on your domain provider.  It may take several minutes for the DNS records to propagate.
1. Go to the `APIs & Services -> Credentials` page and click `Create credentials`.  Select `OAuth client ID`.
1. Click on the `Configure consent screen`.  Select Internal Application Type - this restricts auth to members in your domain.  Chosse an `Application name` - i.e. `Vault Auth Client`.  Add your domain to the `Authorized domains`.  Click `Save`.
1. Choose `Web application` for the Application Type.
1. Provide a name like `Vault Client`.
1. Add `Authorized redirect URIs`.  Add `https://vault.<myorg.com>/ui/vault/auth/oidc/oidc/callback` and `http://localhost:8250/oidc/callback`.  NOTE: the duplicate `oidc` in the URI is not a typo.  It is because the auth provider is mounted at the `/oidc` path.
1. Click `Create`.
1. Take note of the client id and secret that will show up in the completion popup.
1. Run `make bootstrap-vault`.

You should now have a **provisioner** and **admin** token that will last 32 days (default maximum).  These tokens will be used as follows:

- Issue the **admin** token to a human admin.  This is only used for manual Vault management and should not be used for regular user access.
- Issue the **provisioner** token to the `shared` product `VAULT_TOKEN` (see next section)
- Create a new **admin** token for `is-org`, this token needs elevated priviliges to manage Vault policies and ACLs.

1. Go to terraform.io -> prd-is-org workspace and add a new variable VAULT_TOKEN.  Copy the **admin** token into that variable and mark it secret.
1. `main.tf`: Uncomment Vault token usage and comment out GCP_ACCESS_TOKEN
1. `main.tf`: Enabled entities to true for `ENABLE_{VAULT, ORG, PRODUCT, TOKEN}`
1. `auth.tf`: Uncomment OIDC code
1. `gcp_rolesets.tf`: Uncomment everything - used to setup a cloudrun admin service account for Vault - used to setup cloudsql connections
1. Edit `project.auto.tfvars` and add `VAULT_ADDRESS` (note spelling, not the same as the Vault CLI) to match the URL used above to map a domain for OIDC.
1. Add OIDC_CLIENT_ID and OIDC_CLIENT_SECRET to `is-org` workspace ENV, make sure to mark secret.  Use the vaules from above when creatin the OIDC credentials.
1. Commit and push.
1. Review plan and apply.

At this point you should be able to either go to the Vault UI at `https://vault.<myorg.com>`, select OIDC as the authentication method and click "Google".  Login using your account registered to `<myorg.com>` and you will be allowed into Vault.

You can also get a login via the Vault CLI and get a token using: `vault login -method=oidc`

***Congratulations!!!***  You now have a Terraform Workspace, GCP Project and a Vault instance deployed that will allow members of your GCP organization to authenticate.  This particular workspace should be considered special as it is the parent for all projects and controls the foundation of your GCP org.  You're now ready for the next step which is to add the first Product which is a GCP project that holdes shared resources for your organization and keeps them separate from the IS-Org Product.

## Org policies

The `org-policies` module has a layout for default policies.  These policies are required for the Product Factor Module (PFM), but should be reviewed to understand what is being granted as they generally enable access to a GCP roleset.  For example, a `product-prd-website-viewer` could be issued to a network admin to be able to view logs and monitor the production website.  The policies can also be used to form groups in vault to make team based roles for users that authenticate with OIDC.

The Product Owner policy provides an easy way to grant access to Product Workspaces in terraform.io for managing GCP resources.  The first product example, "Shared", will use this default policy to lower the permissions of the Terraform `prd-shared` workspace to prevent unauthorzied/accidental changes to the GCP organisation and Vault configuration.

## Your First Product: Shared

The `is-org` product is a sepcial because the GCP permissions granted to terraform.io are much greater.  This product will be the first example of a typical product of which you will likely have many.  The `shared` product is an example for managing global GCP resources that have "team" read access.  This example will illustrate Vault policy management and adding new projects.

Use Case: You would like to create a global Docker registry for all team members to download published internal software (ex: a Docker image which contains build tools and a local build environment).

Navigate to the `prod/is-org` folder and edit the `products.tf` file.  There is a sample Product commented out, but we will create a new one from the example below.  See [PFM Variables](modules/product-factory/variables.tf) for detailed documentation on each variable to understand what this will do.

```hcl
module "product_shared" {
  source = "git::https://gitlab.com/pennatus/infrastructure//modules/product-factory"

  PRODUCT_NAME = "shared"
  ENVIRONMENTS = [ "prd" ]
  APIS = [ "containerregistry.googleapis.com", "storage-component.googleapis.com" ]
  SSH_KEYS = [ ]
  KV_SECRET_PATHS = [ ]

  ADMIN_PROJECT = var.ADMIN_PROJECT
  BILLING_DISPLAY_NAME = var.BILLING_NAME_GENERAL
  DOMAIN = var.DOMAIN
  FOLDER_IDS = module.org_folders.folder_ids
}

output project_ids {
  value = module.product_shared.env_project_map
}
```

We will need a "team" policy in Vault that provides READ access to a path that issues a temporary GCP service account with read permission.  To do this change `ENABLE_OIDC_DEFAULT` to `true` in `main.tf`.

This is a good time to practice the workflows.  As you are performing org level changes, create a branch, commit, push and submit for review.  After the branch has been reviewed and merged into master, Terraform plan will be triggered and ready for review. Invoke a similar process for the plan, have it reviewed by someone familiar with `is-org` workspace, but preferably not the developer that pushed the change.  After the plan is reviewed, it can be applied which will create the new project(s) in GCP.

It's now time to put this new project under CI/CD:

1. Go to terraform.io and add a new workspace to your organization.  Name it `prd-shared` and configure the monitored directories as `prod/shared` and `modules`.
1. Create a Vault token for this workspace.  It is probably best to create orphan tokens so that if the parent is revoked the child tokens used by the Product workspaces can continue to function.  We will use the policy `product-prd-shared-owner` which was automatically created by the PFM and grants sufficient privileges in Vault to fetch a GCP Project Service Account with the GCP Owner role:  `VAULT_ADDR=https://vault.example.com  vault token create -orphan -policy=product-prd-shared-owner -policy=token-creator`
1. If you are creating a database instance within this product you will need to create the token with some additional policies: `vault token create -orphan -policy=product-prd-shared-owner -policy=token-creator -policy=db-instance-owner`
1. Edit the `shared` workspace variables and add `VAULT_TOKEN` and put the output of this token in there.  Make sure to check "secret".  This token is no longer needed and does not need to be saved.
1. Go to your cloned `infrastructure` respository and create the new product.  The example only created one environment so at the root `mkdir prod/shared` is sufficient.
1. Create the base product terraform files, but no content: `touch prod/shared/{main,variables,outputs}.tf`.
1. Commit/Review/Merge this change and then Plan/Review/Apply and you should have a team viewable Docker registry!

## Next steps

At this point you should be able to manage your organization by writing Terraform to configure resources along and adding Vault config as demonstrated above.  Here is a list of additional areas to review:

1. Manage all your domain records by using GCP Cloud DNS servers.  The umbrella resource could be configured in the Shared product with each Product adding DNS records (resources) if the Product Owner SA is granted the role.
1. Manage internal resources used by more than one project, for example a shared Docker registry or a private PyPi registry.  Note, larger shared projects will likely justify being a separate product so that changes to the Shared Product workspace don't trigger Plan/Review/Apply on critical resources.
1. Create a new Product for a specific application like a website or webapp using the Product Factory Module (PFM).
1. Create a shared database server usable by multiple projects: [PostgresQL on GCP](modules/postgres-db/variables.tf)

Generally changes will fall into one or more categories:

- Create/Delete/Update a set of Product resources in Terraform for each product environments
  - GCP resources
  - Vault paths (Secret Engine mounts, or additional paths specific to a Secret Engine)
- Create/Delete/Update Organisaton (`org-is` product) resources
  - Vault policies to provide product access
  - GCP projects (using PFM)
  - Additional Vault auth endpoints

This allows apps (or any client) to access Vault with a scoped token and read any secrets that are needed.

## Long term maintenance

This project is not considered a production ready environment and it is important to understand the following concepts and adapt to your needs:

- Vault infrastructure and hardening
  - Consider using a helm chart to bring up Vault in Kubernetes
  - Put the Vault URL behind a VPN
  - Consider backup and disaster recovery strategy
- Module maintenance: pinning vs. forking vs. HEAD
  - Using external modules is a risk of code injecton
  - Pinning modules prevents suprise changes
- Vault policy maintenance
  - Reviewing policies is critical to understanding the surface area exposed to attack
